const path = require('path');

const config = {
    theme_name: "custom",
    themes_path: "../src/wp-content/themes/",
    admin_src_path: '/assets/src/admin/',
    src_path: '/assets/src/',
    dist_path: '/assets/dist/',
    vendor_path: '/assets/src/vendor/',
    getSrcPath: function () {
        return path.join(__dirname, `${this.themes_path}${this.theme_name}${this.src_path}`);
    },
    getAdminSrcPath: function () {
        return path.join(__dirname, `${this.themes_path}${this.theme_name}${this.admin_src_path}`);
    },
    getDistPath: function () {
        return path.join(__dirname, `${this.themes_path}${this.theme_name}${this.dist_path}`);
    },
    getVendorPath: function () {
        return path.join(__dirname, `${this.themes_path}${this.theme_name}${this.vendor_path}`);
    },
    getEntry: function() {
        const SRC = this.getSrcPath();
        const ADMIN_SRC = this.getAdminSrcPath();

        return {
            theme: [`${SRC}js/Index.js`],
            admin: [`${ADMIN_SRC}js/Admin.js`],
            babelPolyfill: 'babel-polyfill',
        };
    }
};

module.exports = config;
