const argv = require('yargs').argv;
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');

const IS_DEVELOPMENT = argv.mode === 'development';
const BrowserSyncWebpackPlugin =  IS_DEVELOPMENT ? require('browser-sync-webpack-plugin') : false;

const THEME_CONFIG = require('./theme.config');
const ENV = IS_DEVELOPMENT ? require('./env') : false;
const DIST_PATH = THEME_CONFIG.getDistPath();

const config = {
  entry: THEME_CONFIG.getEntry(),
  output: {
    filename: '[name]-[hash].js',
    path: DIST_PATH
  },
  watch: IS_DEVELOPMENT,
  resolve: {
    alias: {
      vendor: THEME_CONFIG.getVendorPath(),
    }
  },
  module: {
    rules: [{
      test: /\.js$/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: [
            '@babel/preset-env'
          ],
          plugins: [
            ['@babel/plugin-proposal-optional-chaining'],
            ['@babel/plugin-proposal-nullish-coalescing-operator']
          ]
        }
      }
    }, {
      test: /\.(sa|sc|c)ss$/,
      use: [
        MiniCssExtractPlugin.loader,
        'css-loader?sourceMap',
        {
          loader: 'postcss-loader',
          options: {
            ident: 'postcss',
            plugins: [
              require('postcss-smart-import')(),
              require('autoprefixer')(),
              require('postcss-clean')()
            ]
          }
        },
        'sass-loader?sourceMap',
      ]
    }, {
        test: /\.(gif|png|jpe?g|svg)$/i,
        use: [{
            loader: 'file-loader',
            options: {
                name: '[name]-[hash].[ext]'
            }
        }]
    }, {
        test: /\.(eot|ttf|woff|woff2)$/,
        use: {
            loader: 'file-loader',
            options: {
              name: '[name]-[hash].[ext]'
            }
        },
    }]
  },
  plugins: [
    new CleanWebpackPlugin({
      dry:            false,
      verbose:        true,
      watch:          true,
      allowExternal:  false,
      beforeEmit:     false,
      cleanStaleWebpackAssets: false,
    }),
    new MiniCssExtractPlugin({
      filename: '[name]-[hash].css',
      chunkFilename: '[id].css',
      path: DIST_PATH,
      disable: false
    }),
    new ManifestPlugin({
      fileName: 'manifest.json',
      basePath: '',
      writeToFileEmit: true,
      filter: (file) => file.path.match(/\.js|.css$/) && !file.path.match(/\.map$/),
    }),
    IS_DEVELOPMENT ? new BrowserSyncWebpackPlugin({ 
      proxy: ENV.browser_sync_local_url,
      port: 3001,
      open: true
    }) : () => {}
  ],
  optimization: !IS_DEVELOPMENT ? {
    minimize: true,
    minimizer: [new TerserPlugin({
      sourceMap: true,
      terserOptions: {
         extractComments: 'all',
         compress: {
            drop_console: false,
         },
      },
    })],
  } : {},
  devtool: 'source-map',
  devServer: {
    contentBase: DIST_PATH,
    compress: false,
    open: false,
    writeToDisk: true
  }
};

module.exports = config;