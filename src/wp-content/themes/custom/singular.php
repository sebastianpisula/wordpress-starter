<?php
/**
 * Theme template.
 *
 * @package Theme
 */

the_post();
get_header(); ?>

<?php the_title(); ?>
<?php the_content(); ?>

<?php
get_footer();
