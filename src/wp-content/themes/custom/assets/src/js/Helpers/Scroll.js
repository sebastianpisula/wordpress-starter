// GET WINDOW SCROLL
export function getScrollPosition() {
    return {
        top: Math.ceil(window.pageYOffset || document.documentElement.scrollTop),
        left: Math.ceil(window.pageXOffset || document.documentElement.scrollLeft)
    };
}
