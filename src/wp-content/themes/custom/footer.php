<?php
/**
 * Theme template.
 *
 * @package Theme
 */

?>
<footer>
	<div class="container">
		<div class="row">
			<?php dynamic_sidebar( 'bottom' ); ?>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
