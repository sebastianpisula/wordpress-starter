<?php
/**
 * Generate thumbnails on demand
 *
 * @package Theme
 */

namespace Theme\Module;

/**
 * Class Thumbnail_Demand
 *
 * @package Theme\Module
 * @source http://wordpress.stackexchange.com/a/124790
 */
class Thumbnail_Demand {
	/**
	 * Thumbnail_Demand constructor.
	 */
	public function __construct() {
		add_filter( 'image_downsize', [ $this, 'image_downsize' ], 5, 3 );
		add_filter( 'intermediate_image_sizes_advanced', [ $this, 'intermediate_image_sizes_advanced' ], 5, 2 );
	}

	/**
	 * Filters whether to preempt the output of image_downsize().
	 * Passing a truthy value to the filter will effectively short-circuit
	 * down-sizing the image, returning that value as output instead.
	 *
	 * @param bool         $downsize Whether to short-circuit the image downsize. Default false.
	 * @param int          $id       Attachment ID for image.
	 * @param array|string $size     Size of image. Image size or array of width and height values (in that order).
	 *                               Default 'medium'.
	 *
	 * @return array|bool
	 */
	public function image_downsize( $downsize, $id, $size ) {
		global $_wp_additional_image_sizes;

		if ( 'image/svg+xml' === get_post_mime_type( $id ) ) {
			return false;
		}

		// If image size exists let WP serve it like normally.
		$imagedata = wp_get_attachment_metadata( $id );
		if ( empty( $size ) || is_array( $size ) || isset( $imagedata['sizes'][ $size ] ) ) {
			return false;
		}

		// Check that the requested size exists, or abort.
		if ( ! isset( $_wp_additional_image_sizes[ $size ] ) ) {
			return false;
		}

		$excluded_sizes = get_post_meta( $id, '_exclude_size' );

		if ( in_array( $size, $excluded_sizes ) ) {
			return false;
		}

		// Make the new thumb.
		$resized = image_make_intermediate_size(
			get_attached_file( $id ),
			$_wp_additional_image_sizes[ $size ]['width'],
			$_wp_additional_image_sizes[ $size ]['height'],
			$_wp_additional_image_sizes[ $size ]['crop']
		);

		if ( ! $resized ) {
			add_post_meta( $id, '_exclude_size', $size );

			return false;
		}

		// Save image meta, or WP can't see that the thumb exists now.
		$imagedata['sizes'][ $size ] = $resized;
		wp_update_attachment_metadata( $id, $imagedata );

		// Return the array for displaying the resized image.
		$att_url = wp_get_attachment_url( $id );

		return [ dirname( $att_url ) . '/' . $resized['file'], $resized['width'], $resized['height'], true ];
	}

	/**
	 * Filters the image sizes automatically generated when uploading an image.
	 *
	 * @param array $sizes    An associative array of image sizes.
	 * @param array $metadata An associative array of image metadata: width, height, file.
	 *
	 * @return array
	 */
	public function intermediate_image_sizes_advanced( $sizes, $metadata ) {
		return [
			'thumbnail' => $sizes['thumbnail'],
			'medium'    => $sizes['medium'],
			'large'     => $sizes['large'],
		];
	}
}
