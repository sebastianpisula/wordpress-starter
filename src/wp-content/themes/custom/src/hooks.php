<?php
/**
 * Here you can register filters
 *
 * @package Theme
 */

/**
 * Class Hooks
 */
class Hooks {
	/**
	 *
	 */
	public function add_hooks() {
		//Editor Changes
		add_filter( 'mce_buttons', [ $this, 'add_mce_buttons' ] );
		add_filter( 'mce_buttons_2', [ $this, 'add_mce_buttons_2' ] );
		add_filter( 'tiny_mce_before_init', [ $this, 'tiny_mce_before_init' ], 12 );
	}

	/**
	 * @param array $buttons
	 *
	 * @return array
	 */
	public function add_mce_buttons( $buttons ) {
		$new_buttons = [];

		foreach ( $buttons as $button ) {
			$new_buttons[] = $button;

			if ( $button === 'alignright' ) {
				$new_buttons[] = 'alignjustify';
			}
		}

		return $new_buttons;
	}

	/**
	 * @param array $buttons
	 *
	 * @return array
	 */
	public function add_mce_buttons_2( $buttons ) {
		$new_buttons = [];

		foreach ( $buttons as $button ) {
			$new_buttons[] = $button;

			if ( $button === 'hr' ) {
				$new_buttons[] = 'fontselect'; //Font Family
				$new_buttons[] = 'fontsizeselect'; //Font Size
			}
		}

		return $new_buttons;
	}

	/**
	 * @param array $tinymce_settings
	 *
	 * @return array
	 */
	public function tiny_mce_before_init( $tinymce_settings ) {
		$tinymce_settings['font_formats']     = 'Lato=Lato;Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats';
		$tinymce_settings['fontsize_formats'] = "10px 11px 12px 13px 14px 15px 16px 17px 18px 19px 20px 21px 22px 23px 24px 25px 26px 27px 28px 29px 30px 32px";

		return $tinymce_settings;
	}
}

( new Hooks() )->add_hooks();
