<?php
/**
 * Basic Theme
 *
 * @package Theme
 */

namespace Theme\Core;

use Theme\Module\Custom_Class;
use Theme\Module\Disable_Embed;
use Theme\Module\Disable_Emoji;
use Theme\Module\Lazy_Loading;
use Theme\Module\Rewrite_Rules;
use Theme\Module\Thumbnail_Demand;
use WC_Frontend_Scripts;
use WP_Admin_Bar;

/**
 * Class Theme_Base
 *
 * @package Theme\Core
 */
abstract class Base {
	/**
	 * Google API Key
	 *
	 * @var string
	 */
	protected $google_api_key = '';

	/**
	 * Which post type support block editor aka Gutenberg
	 *
	 * @var string[]
	 */
	protected $post_types_supported_gutenberg = [];

	/**
	 * Theme_Base constructor.
	 */
	public function __construct() {
		require get_parent_theme_file_path( 'src/helpers.php' );
		require get_parent_theme_file_path( 'src/hooks.php' );
		require get_parent_theme_file_path( 'src/shortcodes.php' );

		if ( apply_filters( 'theme/module/disable_emoji', true ) ) {
			new Disable_Emoji();
		}

		if ( apply_filters( 'theme/module/disable_embed', true ) ) {
			new Disable_Embed();
		}

		if ( apply_filters( 'theme/module/lazy_loading', true ) ) {
			new Lazy_Loading();
		}

		new Custom_Class();
		new Rewrite_Rules();
		new Thumbnail_Demand();

		add_action( 'after_setup_theme', [ $this, 'setup' ], 5 );
		add_action( 'widgets_init', [ $this, 'register_sidebars' ], 5 );

		add_action( 'admin_bar_menu', [ $this, 'template_details' ], 99 );

		add_action( 'wp_footer', [ $this, 'wp_footer' ], 0 );
		add_action( 'wp_enqueue_scripts', [ $this, 'register_scripts' ], 20 );
		add_action( 'admin_enqueue_scripts', [ $this, 'register_admin_scripts' ], 200 );

		add_action( 'admin_init', [ $this, 'editor_styles' ] );

		add_action( 'theme/register_scripts/before', [ $this, 'register_scripts_before' ], 10, 3 );
		add_action( 'theme/register_scripts/after', [ $this, 'register_scripts_after' ], 10, 3 );

		add_filter( 'theme/deps/style', [ $this, 'style_deps' ] );
		add_filter( 'theme/deps/script', [ $this, 'script_deps' ] );

		add_filter( 'theme/deps/style', [ $this, 'both_deps' ] );
		add_filter( 'theme/deps/script', [ $this, 'both_deps' ] );

		add_filter( 'theme/localize/script', [ $this, 'localize_script' ] );

		add_action( 'widgets_init', [ $this, 'register_widgets' ] );

		add_filter( 'style_loader_src', [ $this, 'loader_src' ], 10, 2 );
		add_filter( 'script_loader_src', [ $this, 'loader_src' ], 10, 2 );

		add_filter( 'wpcf7_load_js', [ $this, 'wpcf7_load_js' ] );
		add_filter( 'wpcf7_load_css', '__return_false' );

		add_filter( 'mime_types', [ $this, 'mime_types' ] );
		add_filter( 'wp_check_filetype_and_ext', [ $this, 'wp_check_filetype_and_ext' ], 10, 5 );
		add_filter( 'jpeg_quality', [ $this, 'jpeg_quality' ] );

		add_filter( 'acf/settings/google_api_key', [ $this, 'google_api_key' ] );

		//Gutenberg.
		add_filter( 'use_block_editor_for_post_type', [ $this, 'remove_gutenberg_support' ], 10, 2 );

		//Yoast Seo
		add_filter( 'wpseo_metabox_prio', [ $this, 'wpseo_metabox_prio' ] );

		//Excerpt.
		add_filter( 'get_the_excerpt', 'trim', 0 );

		// Disable CSS / JS for WPML.
		define( 'ICL_DONT_LOAD_NAVIGATION_CSS', true );
		define( 'ICL_DONT_LOAD_LANGUAGES_JS', true );
		define( 'ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true );

		// Disable CSS for WooCommerce.
		add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );
		add_action( 'template_redirect', [ $this, 'template_redirect' ] );
	}

	/**
	 * Change metabox priority.
	 */
	public function wpseo_metabox_prio() {
		return 'low';
	}

	public function template_redirect() {
		if ( ! function_exists( 'is_woocommerce' ) ) {
			return;
		}

		$allow_woo_scripts = is_checkout() || is_cart();

		if ( ! $allow_woo_scripts ) {
			remove_action( 'wp_enqueue_scripts', [ WC_Frontend_Scripts::class, 'load_scripts' ] );
			remove_action( 'wp_print_scripts', [ WC_Frontend_Scripts::class, 'localize_printed_scripts' ], 5 );
			remove_action( 'wp_print_footer_scripts', [
				WC_Frontend_Scripts::class,
				'localize_printed_scripts'
			], 5 );
		}
	}

	/**
	 * Filter CF7 JS
	 *
	 * @param bool $status Status of display CF7 Scripts.
	 *
	 * @return bool
	 */
	public function wpcf7_load_js( $status ) {
		return $status;
	}

	/**
	 * Remove version for theme scripts
	 *
	 * @param string $src    The source URL of the enqueued style.
	 * @param string $handle The style's registered handle.
	 *
	 * @return string
	 */
	public function loader_src( $src, $handle ) {
		if ( 'main' === $handle ) {
			return remove_query_arg( [ 'ver' ], $src );
		}

		return $src;
	}

	/**
	 * Filters list of allowed mime types and file extensions.
	 *
	 * @param array $mime_types      Mime types keyed by the file extension regex corresponding to
	 *                               those types. 'swf' and 'exe' removed from full list. 'htm|html' also
	 *                               removed depending on '$user' capabilities.
	 *
	 * @return array
	 */
	public function mime_types( $mime_types ) {
		$mime_types['svg'] = 'image/svg+xml';

		return $mime_types;
	}

	public function wp_check_filetype_and_ext( $wp_check_filetype_and_ext, $file, $filename, $mimes, $real_mime ) {
		if ( $real_mime === 'image/svg' ) {
			return [
				'ext'             => 'svg',
				'type'            => $real_mime,
				'proper_filename' => false,
			];
		}

		return $wp_check_filetype_and_ext;
	}

	/**
	 * Default JPG Quality.
	 *
	 * @param int $jpeg_quality Quality level between 0 (low) and 100 (high) of the JPEG.
	 *
	 * @return int
	 */
	public function jpeg_quality( $jpeg_quality ) {
		return 100;
	}

	/**
	 * Filter for api key google maps.
	 *
	 * @return string
	 */
	public function google_api_key() {
		return $this->google_api_key;
	}

	/**
	 * Register admin scripts.
	 */
	public function register_admin_scripts() {
		// Urls.
		$urls = $this->get_urls();

		// Register dirs.
		$dirs = $this->get_dirs();

		/**
		 * Main styles.
		 */
		$filename   = $this->get_asset_file( 'admin.css' );
		$style_full = $dirs['dist'] . $filename;

		if ( file_exists( $style_full ) ) {
			wp_enqueue_style( 'main', $urls['dist'] . $filename );
		}
	}

	/**
	 * Register Widgets.
	 */
	public function register_widgets() {
	}

	/**
	 * Deps for CS and JS.
	 *
	 * @param array $deps An array of registered script handles this script depends on. Default empty array.
	 *
	 * @return array
	 */
	public function both_deps( $deps ) {
		return $deps;
	}

	/**
	 * Deps for JS.
	 *
	 * @param array $deps An array of registered script handles this script depends on. Default empty array.
	 *
	 * @return array
	 */
	public function script_deps( $deps ) {
		return $deps;
	}

	/**
	 * Deps for css.
	 *
	 * @param array $deps An array of registered script handles this script depends on. Default empty array.
	 *
	 * @return array
	 */
	public function style_deps( $deps ) {
		return $deps;
	}

	/**
	 * Main script localize.
	 *
	 * @param array $vars Array vars for JS.
	 *
	 * @return array
	 */
	public function localize_script( $vars ) {
		return $vars;
	}

	/**
	 * Before main script and style.
	 *
	 * @param array $urls Array of theme urls.
	 * @param array $dirs Array of theme dirs.
	 */
	public function register_scripts_before( $urls, $dirs ) {
	}

	/**
	 * After main script and style.
	 *
	 * @param array $urls Array of theme urls.
	 * @param array $dirs Array of theme dirs.
	 */
	public function register_scripts_after( $urls, $dirs ) {
	}

	/**
	 * Settings
	 */
	public function setup() {
		load_theme_textdomain( 'theme', get_template_directory() . '/languages' );

		add_theme_support( 'automatic-feed-links' );

		add_theme_support( 'title-tag' );

		add_theme_support( 'post-thumbnails' );

		add_theme_support( 'post-formats' );

		add_theme_support( 'custom-background' );

		add_theme_support( 'custom-header' );

		add_theme_support( 'custom-logo' );

		add_theme_support( 'customize-selective-refresh-widgets' );

		add_theme_support( 'admin-bar', [ 'callback' => '__return_false' ] );

		add_theme_support( 'html5', [ 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ] );

		add_theme_support( 'woocommerce' );

		add_theme_support( 'wc-product-gallery-slider' );

		add_theme_support( 'wc-product-gallery-lightbox' );

		$this->register_menus();
		$this->add_image_sizes();
	}

	/**
	 * Current template info
	 *
	 * @param WP_Admin_Bar $wp_admin_bar WP_Admin_Bar instance, passed by reference.
	 */
	public function template_details( $wp_admin_bar ) {
		global $template;

		if ( is_admin() ) {
			return;
		}

		$_template   = wp_normalize_path( $template );
		$content_dir = wp_normalize_path( WP_CONTENT_DIR . '/' );
		$_template   = str_replace( $content_dir, '', $_template );

		$wp_admin_bar->add_node( [
			'id'    => 'current-template-file',
			'title' => $_template,
		] );
	}

	/**
	 * Register editor style.
	 */
	public function editor_styles() {
		$filename = $this->get_asset_file( 'editor.css' );

		// Register urls.
		$dirs = $this->get_dirs();

		if ( file_exists( wp_normalize_path( $dirs['dist'] . $filename ) ) ) {
			$urls = $this->get_urls();

			add_editor_style( $urls['dist'] . $filename );
		}
	}

	/**
	 * Get urls.
	 *
	 * @return array
	 */
	public function get_urls() {
		$template_url = trailingslashit( get_template_directory_uri() );

		return [
			'template' => $template_url,
			'assets'   => $template_url . 'assets/',
			'dist'     => $template_url . 'assets/dist/',
			'vendor'   => $template_url . 'assets/vendor/',
		];
	}

	/**
	 * Get dirs.
	 *
	 * @return array
	 */
	public function get_dirs() {
		$dir_path = wp_normalize_path( get_template_directory() . '/' );

		return [
			'template' => $dir_path,
			'assets'   => wp_normalize_path( $dir_path . 'assets/' ),
			'dist'     => wp_normalize_path( $dir_path . 'assets/dist/' ),
			'vendor'   => wp_normalize_path( $dir_path . 'assets/vendor/' ),
		];
	}

	/**
	 * Register script and styles.
	 */
	public function register_scripts() {
		// Gutenberg block disable.
		wp_dequeue_style( 'wp-block-library' ); // Wordpress core
		wp_dequeue_style( 'wp-block-library-theme' ); // Wordpress core
		wp_dequeue_style( 'wc-block-style' ); // WooCommerce
		wp_dequeue_style( 'storefront-gutenberg-blocks' ); // Storefront theme

		wp_deregister_script( 'jquery' );
		wp_register_script( 'jquery', includes_url( '/js/jquery/jquery.js' ), false, null, true );

		// Urls.
		$urls = $this->get_urls();

		// Register dirs.
		$dirs = $this->get_dirs();

		do_action( 'theme/register_scripts/before', $urls, $dirs );

		/**
		 * Main styles.
		 */
		$filename   = $this->get_asset_file( 'theme.css' );
		$style_full = $dirs['dist'] . $filename;

		if ( file_exists( $style_full ) ) {
			wp_enqueue_style( 'main', $urls['dist'] . $filename, apply_filters( 'theme/deps/style', [] ) );
		}

		/**
		 * Main scripts.
		 */
		$filename     = $this->get_asset_file( 'theme.js' );
		$scripts_full = $dirs['dist'] . $filename;
		if ( file_exists( $scripts_full ) ) {
			$vars = [
				'url'      => [
					'home'     => trailingslashit( home_url() ),
					'ajax'     => admin_url( 'admin-ajax.php' ),
					'template' => trailingslashit( get_template_directory_uri() ),
					'api'      => get_rest_url(),
				],
				'locale'   => get_user_locale(),
				'language' => apply_filters( 'wpml_current_language', null ),
				'l10n'     => [],
				'cookie'   => [
					'path'   => COOKIEPATH,
					'domain' => COOKIE_DOMAIN ? COOKIE_DOMAIN : '.' . str_replace( 'www.', '', wp_parse_url( home_url(), PHP_URL_HOST ) ),
					'secure' => is_ssl(),
				],
				'nonce'    => [
					'api' => wp_create_nonce( 'wp_rest' ),
				],
				'dist'     => [
					'babelPolyfill' => $urls['dist'] . $this->get_asset_file( 'babelPolyfill.js' ),
				]
			];

			wp_register_script( 'main', $urls['dist'] . $filename, apply_filters( 'theme/deps/script', [] ), null, true );
			wp_localize_script( 'main', '__jsVars', apply_filters( 'theme/localize/script', $vars ) );
		}

		do_action( 'theme/register_scripts/after', $urls, $dirs );
	}

	/**
	 * Register custom menu.
	 */
	public function register_menus() {
	}

	/**
	 * Register sidebar.
	 */
	public function register_sidebars() {
	}

	/**
	 * Register image sizes.
	 */
	public function add_image_sizes() {
	}

	public function wp_footer() {
		wp_enqueue_script( 'main' );
	}

	/**
	 * Load filename from scheme.
	 *
	 * @param string $file File to load.
	 *
	 * @return string
	 */
	private function get_asset_file( $file ) {
		$manifest_file = get_parent_theme_file_path( 'assets/dist/manifest.json' );

		if ( ! file_exists( $manifest_file ) ) {
			return $file;
		}

		$manifest_array = json_decode( file_get_contents( $manifest_file ), 1 );

		if ( isset( $manifest_array[ $file ] ) ) {
			return $manifest_array[ $file ];
		}

		return $file;
	}

	/**
	 * @param bool   $is_enabled
	 * @param string $post_type
	 *
	 * @return bool
	 */
	public function remove_gutenberg_support( $is_enabled, $post_type ): bool {
		return in_array( $post_type, $this->post_types_supported_gutenberg );
	}
}
