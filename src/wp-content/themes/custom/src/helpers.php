<?php
/**
 * Here you can declare helpers.
 *
 * @package Theme
 */

/**
 * Get an HTML img element representing an image attachment
 * While `$size` will accept an array, it is better to register a size with
 * add_image_size() so that a cropped version is generated. It's much more
 * efficient than having to find the closest-sized image and then having the
 * browser scale down the image.
 *
 * @param int          $attachment_id Image attachment ID.
 * @param string|array $size          Optional. Image size. Accepts any valid image size, or an array of width
 *                                    and height values in pixels (in that order). Default 'thumbnail'.
 * @param bool         $icon          Optional. Whether the image should be treated as an icon. Default false.
 * @param string|array $attr          Optional. Attributes for the image markup. Default empty.
 *
 * @return string HTML img element or empty string on failure.
 */
function ic_get_attachment_image( $attachment_id, $size = 'thumbnail', $icon = false, $attr = '' ) {
	if ( get_post_mime_type( $attachment_id ) === 'image/svg+xml' ) {
		$file = get_attached_file( $attachment_id );

		if ( file_exists( $file ) ) {
			return file_get_contents( $file );
		}
	}

	return wp_get_attachment_image( $attachment_id, $size, $icon, $attr );
}

/**
 * @param int          $image_id
 * @param array|string $sizes
 * @param string       $breakpoint_mode
 */
function ic_background_image_attribute( $image_id, $sizes = 'full', $breakpoint_mode = 'block' ) {
	$value = esc_attr( apply_filters( 'theme/lazy_loading/image_attributes', '', $image_id, $sizes, $breakpoint_mode ) );

	if ( $value ) {
		echo sprintf( " data-performance-load=\"%s\" ", $value );
	}
}

/**
 * @param string $selector
 *
 * @return false|mixed
 */
function ic_get_sub_field( $selector ) {
	global $section_id;

	return $section_id ? get_field( $selector, $section_id ) : get_sub_field( $selector );
}

/**
 * @param string $selector
 * @param mixed  $post_id
 */
function ic_flexible_content( $selector = 'post_content', $post_id = false ) {
	get_template_part( 'page-templates/partials/flexible-content', null, get_defined_vars() );
}
