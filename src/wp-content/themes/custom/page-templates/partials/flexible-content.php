<?php
/** @var array $args */

while ( have_rows( $args['selector'], $args['post_id'] ) ) {
	the_row();

	$layout = get_row_layout();

	$status = get_template_part( 'page-sections/' . $layout );

	if ( $status === false && current_user_can( 'administrator' ) ) {
		trigger_error( sprintf( __( 'Layout %s not exists', 'theme' ), $layout ) );
	}
}
