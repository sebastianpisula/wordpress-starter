<?php

/**
 * Plugin Name: ACF Blocks Manager
 */
class IC_ACF_Blocks {
	/**
	 * IC_ACF_Fields constructor.
	 */
	public function __construct() {
		add_action( 'acf/init', [ $this, 'init' ] );
	}

	/**
	 *
	 */
	public function init() {
		$blocks = wp_get_theme()->get_page_templates( null, 'ic-acf-block' );

		foreach ( $blocks as $file => $name ) {
			if ( $name === 'Example' ) {
				continue;
			}

			$data = get_file_data( get_parent_theme_file_path( $file ), [
				'category' => 'Block Category',
				'title'    => 'Template Name',
				'icon'     => 'Block Icon',
				'keywords' => 'Block Keywords',
			], 'acf_blocks' );

			$data['keywords'] = array_filter( wp_parse_list( $data['keywords'] ) );

			$args = array_merge( $data, [
				'name'            => wp_basename( $file, '.php' ),
				'render_template' => $file,
			] );

			acf_register_block_type( $args );
		}
	}
}

new IC_ACF_Blocks;
