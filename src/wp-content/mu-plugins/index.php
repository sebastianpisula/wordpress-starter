<?php
/**
 * Plugin Name: Little changes
 */

add_filter( 'sanitize_file_name', function ( $filename ) {
	return preg_replace( '/[^a-zA-Z0-9.\- ]/', '', str_replace( '_', '-', mb_strtolower( $filename, 'UTF-8' ) ) );
}, 100 );

/** HELPERS */

/**
 * Check AJAX request.
 *
 * @return bool
 */
function ic_is_ajax_request(): bool {
	return ! empty( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) == 'xmlhttprequest';
}

/**
 * @return int
 */
function ic_get_current_page_num(): int {
	return (int) max( [ get_query_var( 'paged', 1 ), 1 ] );
}