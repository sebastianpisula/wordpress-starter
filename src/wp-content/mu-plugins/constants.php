<?php

add_action( 'muplugins_loaded', function () {
	//Redis
	define( 'WP_CACHE_KEY_SALT', strtoupper( wp_get_environment_type() ) );
} );