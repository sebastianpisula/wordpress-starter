<?php

/**
 * Plugin Name: ACF Fields Manager
 */
class IC_ACF_Fields {
	/**
	 * IC_ACF_Fields constructor.
	 */
	public function __construct() {
		add_action( 'acf/init', [ $this, 'init' ] );

		add_filter( 'acf/settings/autoload', '__return_true' );
		add_filter( 'acf/settings/save_json', [ $this, 'save_json' ], 0 );
		add_filter( 'acf/settings/load_json', [ $this, 'load_json' ], 0 );

		//ACF Fields
		add_filter( 'acf/prepare_field', [ $this, 'acf_prepare_field' ] );
		add_action( 'acf/render_field_settings', [ $this, 'acf_render_field_settings' ] );
	}

	/**
	 * Filters the settings for a particular widget instance.
	 *
	 * @param array     $instance The current widget instance's settings.
	 * @param WP_Widget $widget   The current widget instance.
	 * @param array     $args     An array of default widget arguments.
	 *
	 * @return array
	 */
	public function add_acf_fields( $instance, $widget, $args ) {
		if ( $fields = get_fields( 'widget_' . $widget->id ) ) {
			$instance = array_merge( $instance, $fields );
		}

		return $instance;
	}

	/**
	 *
	 */
	public function init() {
		add_action( 'current_screen', [ $this, 'current_screen' ], 100 );

		//Widget
		add_filter( 'widget_display_callback', [ $this, 'add_acf_fields' ], 100, 3 );
	}

	/**
	 * @param WP_Screen $current_screen
	 */
	public function current_screen( $current_screen ) {
		if ( ! acf_is_screen( 'edit-acf-field-group' ) && ! acf_is_screen( 'acf-field-group' ) ) {
			return;
		}

		$max_input_vars = (int) ini_get( 'max_input_vars' );

		if ( $max_input_vars < 10000 && ic_is_development() ) {
			acf_add_admin_notice( sprintf( '<p><strong>Warning!</strong><br />The variable <code>max_input_vars</code> in the server configuration settings have the value <code>%d</code> - might cause <strong>lose your data</strong> when the website is too big which has too many fields defined by Advanced Custom Fields PRO. Please increase to <code>10000</code>.</p>', $max_input_vars ), 'error' );
		}

		$acf_sync = acf_get_instance( 'ACF_Admin_Field_Groups' )->sync;

		if ( $acf_sync ) {
			acf_add_admin_notice( __( 'Before editing please sync field groups.' ), 'error' );
		}
	}

	/**
	 * @param array $field
	 */
	public function acf_render_field_settings( $field ) {
		acf_render_field_setting( $field, [
			'label'         => __( 'Active?' ),
			'instructions'  => '',
			'name'          => 'status',
			'type'          => 'true_false',
			'default_value' => 1,
			'ui'            => 1,
		], true );
	}

	/**
	 * @param array $field
	 *
	 * @return bool|array
	 */
	public function acf_prepare_field( $field ) {
		if ( isset( $field['status'] ) && ! $field['status'] ) {
			return false;
		}

		return $field;
	}

	/**
	 * @return string
	 */
	public function get_path() {
		return wp_normalize_path( WP_CONTENT_DIR . '/acf-fields' );
	}

	/**
	 * @param string $path
	 *
	 * @return string
	 */
	public function save_json( $path ) {
		return $this->get_path();
	}

	/**
	 * @param array $paths
	 *
	 * @return array
	 */
	public function load_json( $paths ) {
		$paths[] = $this->get_path();

		return $paths;
	}
}

new IC_ACF_Fields;
