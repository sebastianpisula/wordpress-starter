<?php
/**
 * Plugin Name: Cache Menu
 */

class IC_Cache_Menu {
	/**
	 * IC_Cache_Menu constructor.
	 */
	public function __construct() {
		add_filter( 'wp_nav_menu', [ $this, 'wp_nav_menu' ], 10, 2 );
		add_filter( 'pre_wp_nav_menu', [ $this, 'pre_wp_nav_menu' ], 10, 2 );
		add_action( 'wp_update_nav_menu', [ $this, 'wp_update_nav_menu' ], 10, 2 );
	}

	/**
	 * Filters the HTML content for navigation menus.
	 *
	 * @param string   $nav_menu The HTML content for the navigation menu.
	 * @param stdClass $args     An object containing wp_nav_menu() arguments.
	 *
	 * @return string
	 */
	public function wp_nav_menu( $nav_menu, $args ) {
		if ( is_admin() ) {
			return $nav_menu;
		}

		if ( is_user_logged_in() ) {
			return $nav_menu;
		}

		if ( $args->menu ) {
			update_option( 'nav_menu_' . $args->menu->term_id, $nav_menu, 'yes' );
		}

		if ( $args->theme_location ) {
			update_option( 'nav_menu_' . $args->theme_location, $nav_menu, 'yes' );
		}

		return $nav_menu;
	}

	/**
	 * Fires after a navigation menu has been successfully updated.
	 *
	 * @param int   $menu_id   ID of the updated menu.
	 * @param array $menu_data An array of menu data.
	 */
	public function wp_update_nav_menu( $menu_id, $menu_data = [] ) {
		$locations = array_flip( (array) get_theme_mod( 'nav_menu_locations' ) );

		if ( isset( $locations[ $menu_id ] ) ) {
			delete_option( 'nav_menu_' . $locations[ $menu_id ] );
		}

		delete_option( 'nav_menu_' . $menu_id );
	}

	/**
	 * Filters whether to short-circuit the wp_nav_menu() output.
	 *
	 * @param string   $output Nav menu output to short-circuit with. Default null.
	 * @param stdClass $args   An object containing wp_nav_menu() arguments.
	 *
	 * @return string
	 */
	public function pre_wp_nav_menu( $output, $args ) {
		if ( is_admin() ) {
			return $output;
		}

		if ( is_user_logged_in() ) {
			return $output;
		}

		$field = null;

		if ( $args->menu ) {
			$field = $args->menu->term_id;
		} elseif ( $args->theme_location ) {
			$field = $args->theme_location;
		}

		if ( $menu = get_option( 'nav_menu_' . $field ) ) {
			return $menu;
		}

		return $output;
	}
}

new IC_Cache_Menu;
