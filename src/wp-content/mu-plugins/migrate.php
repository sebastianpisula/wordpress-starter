<?php
/**
 * Plugin Name: Migrate DB Config
 */

if ( ! defined( 'WP_CLI' ) || ! WP_CLI || ! defined( 'WP_CONNECTION_CODE' ) || ! defined( 'WP_REMOTE_URL' ) || ! defined( 'WP_REPLACE_URL' ) ) {
	return;
}

add_filter( 'pre_option_wpsdb_settings', function () {
	return array(
		'max_request'       => 1048576,
		'allow_pull'        => true,
		'allow_push'        => false,
		'profiles'          => array(
			0 => array(
				'save_computer'                 => '1',
				'gzip_file'                     => '1',
				'replace_guids'                 => '1',
				'exclude_spam'                  => '0',
				'keep_active_plugins'           => '0',
				'create_backup'                 => '0',
				'exclude_post_types'            => '0',
				'action'                        => 'pull',
				'connection_info'               => WP_REMOTE_URL . PHP_EOL . WP_CONNECTION_CODE,
				'replace_old'                   => array( 1 => untrailingslashit( WP_REPLACE_URL ), ),
				'replace_new'                   => array( 1 => untrailingslashit( home_url() ), ),
				'table_migrate_option'          => 'migrate_only_with_prefix',
				'exclude_transients'            => '1',
				'backup_option'                 => 'backup_only_with_prefix',
				'save_migration_profile'        => '1',
				'save_migration_profile_option' => 'new',
				'create_new_profile'            => 'Pull from dev',
				'name'                          => 'Pull from dev',
			)
		),
		'verify_ssl'        => false,
		'blacklist_plugins' => array(),
	);
} );