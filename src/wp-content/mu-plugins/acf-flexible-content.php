<?php

/**
 * Class IC_Flexible_Content
 */
class IC_Flexible_Content {
	/**
	 * Register hooks.
	 */
	public function add_hooks() {
		add_action( 'init', [ $this, 'init' ] );

		// Additional group settings.
		add_action( 'acf/trash_field_group', [ $this, 'acf_trash_field_group', ] );
		add_action( 'acf/render_field_group_settings', [ $this, 'acf_render_field_group_settings', ] );

		// Rules Location.
		add_filter( 'acf/location/rule_types', [ $this, 'acf_location_rules_types', ] );
		add_filter( 'acf/location/rule_match', [ $this, 'acf_location_rule_match', ], 10, 4 );
		add_filter( 'acf/location/rule_values', [ $this, 'acf_location_rule_values', ], 10, 2 );
		add_filter( 'acf/location/rule_operators', [ $this, 'acf_location_rules_operators', ], 10, 2 );

		// Exporter.
		add_filter( 'acf/prepare_field_group_for_export', [ $this, 'acf_prepare_field_group_for_export', ] );

		add_filter( 'acf/prepare_field/type=flexible_content', [ $this, 'filter_flexible_content_sections', ] );
	}

	public function init() {
		register_post_type( 'ic-section', [
			'label'               => __( 'Sections', 'fpwd' ),
			'supports'            => [ 'title', 'revisions' ],
			'hierarchical'        => false,
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => 'themes.php',
			'menu_position'       => 5,
			'show_in_admin_bar'   => false,
			'show_in_nav_menus'   => false,
			'can_export'          => true,
			'has_archive'         => false,
			'exclude_from_search' => true,
			'publicly_queryable'  => false,
			'rewrite'             => false,
			'capability_type'     => 'page',
			'show_in_rest'        => false,
		] );
	}

	/**
	 * @param array $field
	 *
	 * @return array
	 */
	public function filter_flexible_content_sections( $field ) {
		if ( ! isset( $field['layouts'] ) || ! is_array( $field['layouts'] ) ) {
			return $field;
		}

		$exclude_fields = apply_filters( 'theme/flexible_content/exclude_layouts', [], $field['layouts'], get_current_screen() );

		$field['layouts'] = array_filter( $field['layouts'], function ( $layout ) use ( $exclude_fields ) {
			return ! in_array( $layout['name'], $exclude_fields );
		} );

		return $field;
	}

	/**
	 * @param array $field_group
	 */
	public function acf_trash_field_group( $field_group ) {
		if ( ! isset( $field_group['flexible_content'] ) ) {
			return;
		}

		$this->save_section_fields( $field_group );
	}

	/**
	 * @param array $field_group
	 *
	 * @return array
	 */
	public function acf_prepare_field_group_for_export( $field_group ) {
		if ( $this->is_section_group( $field_group ) ) {
			$this->save_section_fields( $field_group );
		}

		return $field_group;
	}

	/**
	 * @param array $field_group
	 */
	private function save_section_fields( $field_group ) {
		if ( ! isset( $field_group['flexible_content'] ) ) {
			return;
		}
		foreach ( $field_group['flexible_content'] as $flexible_content ) {
			$group = $this->get_flexible_content_json( $flexible_content );

			if ( $group ) {
				acf_write_json_field_group( $group );
			}
		}

	}

	/**
	 * @param string $flexible_content
	 *
	 * @return array
	 */
	private function get_flexible_content_json( $flexible_content ) {
		$field = acf_get_field( $flexible_content );
		$group = acf_get_field_group( $field['parent'] );

		$group['fields'] = acf_get_fields( $field['parent'] );

		$layouts = [
			'layout_5da7200a6839d' => array(
				'key'        => 'layout_5da7200a6839d',
				'name'       => 'section',
				'label'      => 'Defined Section',
				'display'    => 'block',
				'sub_fields' => [
					[
						'key'               => 'field_5da7210d74edf',
						'label'             => 'Section',
						'name'              => 'section_id',
						'prefix'            => 'acf',
						'type'              => 'post_object',
						'value'             => null,
						'menu_order'        => 0,
						'instructions'      => '',
						'required'          => 1,
						'id'                => '',
						'class'             => '',
						'conditional_logic' => 0,
						'wrapper'           => [
							'width' => '',
							'class' => '',
							'id'    => '',
						],
						'status'            => 1,
						'post_type'         => [ 0 => 'ic-section', ],
						'taxonomy'          => '',
						'allow_null'        => 0,
						'multiple'          => 0,
						'return_format'     => 'id',
						'ui'                => 1,
					],
				],
				'min'        => '',
				'max'        => '',
			),
		];

		foreach ( $this->get_sections( $flexible_content ) as $section ) {
			$key = substr( md5( serialize( $section ) ), 0, 13 );

			$layouts[ sprintf( "layout_%s", $key ) ] = [
				'key'        => sprintf( "layout_%s", $key ),
				'name'       => $section['slug'],
				'label'      => $section['name'],
				'display'    => 'block',
				'sub_fields' => [
					[
						'key'               => sprintf( "field_%s", $key ),
						'label'             => $section['slug'],
						'name'              => $section['name'],
						'type'              => 'clone',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => [
							'width' => '',
							'class' => '',
							'id'    => '',
						],
						'clone'             => [
							$section['key'],
						],
						'display'           => 'seamless',
						'layout'            => 'block',
						'prefix_label'      => 0,
						'prefix_name'       => 0,
					],
				],
				'min'        => '',
				'max'        => '',
			];
		}
		$group['fields'][0]['layouts'] = $layouts;

		return $group;
	}

	/**
	 * @param bool  $result
	 * @param array $rule
	 * @param array $screen
	 * @param array $field_group
	 *
	 * @return bool
	 */
	public function acf_location_rule_match( $result, $rule, $screen, $field_group ) {
		if ( $rule['param'] !== 'section' ) {
			return $result;
		}

		if ( ! isset( $screen['post_id'] ) ) {
			return $result;
		}

		$template = wp_doing_ajax() && isset( $_POST['page_template'] ) ? $_POST['page_template'] : get_page_template_slug( $screen['post_id'] );

		return isset( $rule['value'] ) && $template === $rule['value'];
	}

	/**
	 * Add section to choose.
	 *
	 * @param array $values
	 * @param array $rule
	 *
	 * @return array
	 */
	public function acf_location_rule_values( $values, $rule ) {
		if ( $rule['param'] === 'section' ) {
			return wp_get_theme()->get_page_templates( null, 'ic-section' );
		}

		return $values;
	}

	/**
	 * Add new location rule.
	 *
	 * @param array $groups
	 *
	 * @return array
	 */
	function acf_location_rules_types( $groups ) {
		$groups[ __( 'Flexible Content', 'acf' ) ]['section']       = 'Section';
		$groups[ __( 'Flexible Content', 'acf' ) ]['section-field'] = 'Section Field';

		return $groups;
	}

	/**
	 * Modify rule.
	 *
	 * @param array $operators
	 * @param array $rule
	 *
	 * @return array
	 */
	function acf_location_rules_operators( $operators, $rule ) {
		if ( $rule['param'] === 'section' ) {
			return [ '==' => __( "is equal to", 'acf' ) ];
		}

		return $operators;
	}

	/**
	 * @param array $field_group
	 */
	public function acf_render_field_group_settings( $field_group ) {
		$choices = $this->get_flexible_content_fields_choices();

		if ( ! $choices ) {
			return;
		}

		acf_render_field_wrap( [
			'label'        => 'Flexible Content',
			'instructions' => '',
			'type'         => 'checkbox',
			'name'         => 'flexible_content',
			'prefix'       => 'acf_field_group',
			'value'        => isset( $field_group['flexible_content'] ) ? $field_group['flexible_content'] : '',
			'choices'      => $choices,
		] );
	}

	/**
	 * Get fields type flexible content.
	 *
	 * @return array
	 */
	private function get_flexible_content_fields_choices() {
		$fields = [];

		foreach ( acf_get_field_groups() as $group ) {
			$group_fields = acf_get_fields( $group['key'] );

			foreach ( $group_fields as $field ) {
				if ( $field['type'] === 'flexible_content' ) {
					$fields[ $field['key'] ] = sprintf( '%s / %s', $group['title'], $field['label'] );
				}
			}
		}

		return $fields;
	}

	/**
	 * Check acf group is section.
	 *
	 * @param array $field_group
	 *
	 * @return bool
	 */
	private function is_section_group( $field_group ) {
		if ( ! isset( $field_group['location'][0][0]['param'] ) ) {
			return false;
		}

		return $field_group['location'][0][0]['param'] === 'section';
	}

	/**
	 * @param string $flexible_content
	 *
	 * @return array
	 */
	private function get_sections( $flexible_content ) {
		$sections = [];

		foreach ( acf_get_field_groups() as $field_group ) {
			if ( ! isset( $field_group['flexible_content'] ) ) {
				continue;
			}

			if ( ! is_array( $field_group['flexible_content'] ) ) {
				continue;
			}

			if ( ! in_array( $flexible_content, $field_group['flexible_content'] ) ) {
				continue;
			}

			if ( ! $this->is_section_group( $field_group ) ) {
				continue;
			}

			$theme = $field_group['location'][0][0]['value'];
			$file  = get_parent_theme_file_path( $theme );

			if ( ! file_exists( $file ) ) {
				continue;
			}

			$data = get_file_data( $file, [
				'name' => 'Template Name',
			] );

			if ( empty( $data['name'] ) ) {
				continue;
			}

			$sections[ $theme ] = [
				'key'  => $field_group['key'],
				'name' => $data['name'],
				'slug' => wp_basename( $theme, '.php' ),
			];
		}

		return $sections;
	}
}

( new IC_Flexible_Content() )->add_hooks();
