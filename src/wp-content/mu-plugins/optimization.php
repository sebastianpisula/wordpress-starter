<?php
/**
 * Plugin Name: Better Performance
 */

class IC_Optimization {
	/**
	 * IC_Optimization constructor.
	 */
	public function __construct() {
		add_filter( 'mod_rewrite_rules', [ $this, 'mod_rewrite_rules' ] );
	}

	public function mod_rewrite_rules( $rules ) {

		$rules = "AddDefaultCharset UTF-8

<IfModule mod_headers.c>
    Header set Connection keep-alive
    Header unset ETag

    <FilesMatch \".(gz|html|xml|js|css|svg)$\">
        Header append Vary: Accept-Encoding
    </FilesMatch>

    <FilesMatch \"\.(ttf|otf|eot|woff|svg)$\">
        Header set Access-Control-Allow-Origin \"*\"
    </FilesMatch>
</IfModule>
FileETag None

<IfModule mod_expires.c>
    ExpiresActive on
 
    ExpiresDefault                          \"access plus 1 month\"
    ExpiresByType text/cache-manifest       \"access plus 0 seconds\"
    ExpiresByType text/html                 \"access plus 0 seconds\"
    ExpiresByType text/xml                  \"access plus 0 seconds\"
    ExpiresByType application/xml           \"access plus 0 seconds\"
    ExpiresByType application/json          \"access plus 0 seconds\"
    ExpiresByType application/rss+xml       \"access plus 1 hour\"
    ExpiresByType application/atom+xml      \"access plus 1 hour\"
    ExpiresByType image/x-icon              \"access plus 1 week\"
    ExpiresByType image/gif                 \"access plus 1 month\"
    ExpiresByType image/png                 \"access plus 1 month\"
    ExpiresByType image/jpeg                \"access plus 1 month\"
    ExpiresByType image/webp                \"access plus 1 month\"
    ExpiresByType video/ogg                 \"access plus 1 month\"
    ExpiresByType audio/ogg                 \"access plus 1 month\"
    ExpiresByType video/mp4                 \"access plus 1 month\"
    ExpiresByType video/webm                \"access plus 1 month\"
    ExpiresByType text/x-component          \"access plus 1 month\"
    ExpiresByType application/x-font-ttf    \"access plus 1 month\"
    ExpiresByType font/opentype             \"access plus 1 month\"
    ExpiresByType application/x-font-woff   \"access plus 1 month\"
    ExpiresByType application/font-woff2   \"access plus 1 month\"
    ExpiresByType image/svg+xml             \"access plus 1 month\"
    ExpiresByType application/vnd.ms-fontobject \"access plus 1 month\"
    ExpiresByType text/css                  \"access plus 1 year\"
    ExpiresByType application/javascript    \"access plus 1 year\"
 
</IfModule>

<IfModule mod_deflate.c>
	AddType image/svg+xml svg svgz
	AddType application/font-woff2 .woff2

	AddOutputFilterByType DEFLATE text/plain
	AddOutputFilterByType DEFLATE text/html
	AddOutputFilterByType DEFLATE text/xml
	AddOutputFilterByType DEFLATE application/x-httpd-php
	AddOutputFilterByType DEFLATE text/css
	AddOutputFilterByType DEFLATE application/xml
	AddOutputFilterByType DEFLATE application/xhtml+xml
	AddOutputFilterByType DEFLATE image/svg+xml
	AddOutputFilterByType DEFLATE application/rss+xml
	AddOutputFilterByType DEFLATE application/javascript
	AddOutputFilterByType DEFLATE application/x-javascript
	AddOutputFilterByType DEFLATE font/otf
	AddOutputFilterByType DEFLATE font/ttf
	AddOutputFilterByType DEFLATE application/font-woff2
</IfModule>

<IfModule mod_gzip.c>
	mod_gzip_on Yes
	mod_gzip_dechunk Yes
	mod_gzip_item_include file .(html?|txt|css|js|php|pl)$
	mod_gzip_item_include handler ^cgi-script$
	mod_gzip_item_include mime ^text/.*
	mod_gzip_item_include mime ^application/x-javascript.*
	mod_gzip_item_exclude mime ^image/.*
	mod_gzip_item_exclude rspheader ^Content-Encoding:.*gzip.*
</IfModule>\n\n" . $rules;

		return $rules;
	}
}

new IC_Optimization;