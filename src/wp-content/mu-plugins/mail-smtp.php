<?php
/**
 * Plugin Name: SMTP Configuration
 */

class IC_Mail {
	private $wp_mail = [];

	/**
	 *
	 */
	public function add_hooks() {
		//Mail
		add_filter( 'wp_mail', [ $this, 'wp_mail' ], 100 );
		add_filter( 'wp_mail_from', [ $this, 'wp_mail_from' ] );
		add_filter( 'wp_mail_from_name', [ $this, 'wp_mail_from_name' ] );
		add_filter( 'wp_mail_content_type', [ $this, 'wp_mail_content_type' ] );
		add_action( 'phpmailer_init', [ $this, 'phpmailer_init' ], 1000 );
		add_action( 'wp_mail_failed', [ $this, 'wp_mail_failed' ] );
	}

	/**
	 * @param array $args
	 *
	 * @return array
	 */
	public function wp_mail( $args ) {
		$this->wp_mail = $args;

		return $args;
	}

	/**
	 * @param WP_Error $error
	 */
	public function wp_mail_failed( $error ) {
		trigger_error( $error->get_error_message() . ': ' . serialize( $error->error_data ) );
	}

	/**
	 * @return string
	 */
	public function wp_mail_content_type() {
		return 'text/html';
	}

	/**
	 * Fires after PHPMailer is initialized.
	 *
	 * @param PHPMailer $phpmailer The PHPMailer instance (passed by reference).
	 */
	public function phpmailer_init( $phpmailer ) {
		if ( ! $this->get_const_value( 'SMTP_ENABLED', false ) ) {
			return;
		}

		$phpmailer->IsSMTP();

		$phpmailer->Host       = $this->get_const_value( 'SMTP_HOST' );
		$phpmailer->SMTPAuth   = $this->get_const_value( 'SMTP_AUTH', true );
		$phpmailer->Port       = $this->get_const_value( 'SMTP_PORT', 465 );
		$phpmailer->Username   = $this->get_const_value( 'SMTP_AUTH_LOGIN' );
		$phpmailer->Password   = $this->get_const_value( 'SMTP_AUTH_PASSWORD' );
		$phpmailer->SMTPSecure = $this->get_const_value( 'SMTP_SECURE', 'ssl' );

		if ( ! isset( $this->wp_mail['headers']['Reply-To'] ) ) {
			$phpmailer->addReplyTo( $this->get_const_value( 'SMTP_REPLY_EMAIL' ), $this->get_const_value( 'SMTP_REPLY_NAME' ) );
		}
	}

	/**
	 * Filters the email address to send from.
	 *
	 * @param string $from_email Email address to send from.
	 *
	 * @return string
	 */
	public function wp_mail_from( $from_email ) {
		return $this->get_const_value( 'SMTP_FROM_EMAIL', $from_email );
	}

	/**
	 * Filters the name to associate with the "from" email address.
	 *
	 * @param string $from_name Name associated with the "from" email address.
	 *
	 * @return string
	 */
	public function wp_mail_from_name( $from_name ) {
		return $this->get_const_value( 'SMTP_FROM_NAME', $from_name );
	}

	/**
	 * @param string $name
	 * @param string $default
	 *
	 * @return mixed|string
	 */
	private function get_const_value( $name, $default = '' ) {
		return defined( $name ) ? constant( $name ) : $default;
	}
}

( new IC_Mail() )->add_hooks();