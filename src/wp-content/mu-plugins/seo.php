<?php
/**
 * Plugin Name: Better SEO
 */

class IC_Seo {
	/**
	 * IC_Seo constructor.
	 */
	public function __construct() {
		add_filter( 'mod_rewrite_rules', [ $this, 'mod_rewrite_rules' ] );
		add_action( 'template_redirect', [ $this, 'template_redirect' ], 0 );

		add_filter( 'wp_sitemaps_enabled', '__return_false' );

		remove_action( 'wp_head', 'wp_shortlink_wp_head', 10 );
		remove_action( 'template_redirect', 'wp_shortlink_header', 11 );
	}

	public function template_redirect() {
		global $wp;

		$invalid_urls = [ 'index.html', 'index.htm', 'index.php' ];

		$request = ltrim( ! empty( $wp->request ) ? $wp->request : $_SERVER['REQUEST_URI'] ?? '', '/' );

		if ( in_array( $request, $invalid_urls ) ) {
			wp_redirect( $this->home_url(), 301 );
			die();
		}
	}

	/**
	 * Rewrite rules
	 *
	 * @param string $rules
	 *
	 * @return string
	 */
	public function mod_rewrite_rules( $rules ) {
		if ( ! ic_is_production() ) {
			return $rules;
		}

		$new_rules = "<IfModule mod_rewrite.c>\n\tRewriteEngine On\n";

		//Redirect from index.html / index.php / index.htm to homepage
		#$new_rules .= "\tRewriteRule ^index\.(php|html|htm?)$ " . $this->home_url() . " [R=301,L]\n\n";

		$pre = is_ssl() ? 'https://' : 'http://';
		$pre = $this->is_www() ? $pre . 'www.' : $pre . '';

		//WWW redirect
		if ( $this->is_www() ) {
			$new_rules .= "\tRewriteCond %{HTTP_HOST} !^www\. [NC]\n\tRewriteRule ^(.*)$ " . $pre . "%{HTTP_HOST}/$1 [R=301,L]";
		} else {
			$new_rules .= "\tRewriteCond %{HTTP_HOST} ^www\.(.*)$ [NC]\n\tRewriteRule ^(.*)$ " . $pre . "%1/$1 [R=301,L]";
		}

		$new_rules .= "\n\n";

		//Protocol redirect
		if ( is_ssl() ) {
			$new_rules .= "\tRewriteCond %{HTTPS} !=on\n\tRewriteRule ^.*$ " . $pre . "%{SERVER_NAME}%{REQUEST_URI} [R=301,L]";
			#$new_rules .= "\tRewriteCond %{HTTPS} !=on\n\tRewriteRule ^(.*)$ " . $pre . "%{SERVER_NAME}%{REQUEST_URI} [R=301,L]";
		} else {
			$new_rules .= "\tRewriteCond %{HTTPS} on\n\tRewriteRule (.*) " . $pre . "%{HTTP_HOST}%{REQUEST_URI} [R=301,L]";
		}

		$new_rules .= "\n\n";

		//IP Redirect
		$new_rules .= "\tRewriteCond %{HTTP_HOST} ^" . str_replace( '.', '\.', $_SERVER['SERVER_ADDR'] ) . "\n\tRewriteRule (.*) " . $this->home_url() . "$1 [R=301,L]";

		$new_rules .= "\n";

		$new_rules .= "</IfModule>";

		return $new_rules . "\n\n" . $rules;
	}

	/**
	 * @return bool
	 */
	private function is_www() {
		return substr( wp_parse_url( home_url(), PHP_URL_HOST ), 0, 4 ) === 'www.';
	}

	/**
	 * @return string
	 */
	private function home_url() {
		return trailingslashit( home_url() );
	}
}

new IC_Seo();
