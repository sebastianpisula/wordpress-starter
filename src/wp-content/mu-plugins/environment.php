<?php
/**
 * Plugin Name: Environment Configuration
 */

/**
 * Check environment is local.
 *
 * @return bool
 */
function ic_is_local() {
	return wp_get_environment_type() === 'local';
}

/**
 * Check environment is development.
 *
 * @return bool
 */
function ic_is_development() {
	return wp_get_environment_type() === 'development';
}

/**
 * Check environment is staging.
 *
 * @return bool
 */
function ic_is_staging() {
	return wp_get_environment_type() === 'staging';
}

/**
 * Check environment is production
 *
 * @return bool
 */
function ic_is_production() {
	return wp_get_environment_type() === 'production';
}

/**
 * Class IC_Environment
 */
class IC_Environment {
	/** @var string */
	private $auth_login = 'dev';

	/** @var string */
	private $auth_pass = 'dev';

	/**
	 * @return bool
	 */
	private function exists_wp_config() {
		return file_exists( ABSPATH . 'wp-config.php' ) || file_exists( dirname( ABSPATH ) . '/wp-config.php' );
	}

	public function __construct() {
		if ( $this->exists_wp_config() && get_option( 'siteurl' ) && ! defined( 'WP_ENVIRONMENT_TYPE' ) ) {
			wp_die( 'Constant <code>WP_ENVIRONMENT_TYPE</code> is required. Please add to your wp-config.php file.' );
		}

		//Disable ACF in admin menu
		add_filter( 'acf/settings/show_admin', 'ic_is_local' );

		//Privileges and Roles
		add_filter( 'user_has_cap', [ $this, 'user_has_cap' ], 10, 4 );

		//Uploads from remote server
		add_filter( 'mod_rewrite_rules', [ $this, 'mod_rewrite_rules' ] );

		//Block emails
		add_filter( 'wp_mail', [ $this, 'wp_mail' ], 1000 );

		//Admin Bar
		add_action( 'admin_bar_menu', [ $this, 'admin_bar_menu' ], 1000 );

		//Basic Auth
		add_action( 'template_redirect', [ $this, 'template_redirect' ] );

		//Custom Headers
		add_filter( 'wp_headers', [ $this, 'wp_headers' ] );

		//Modify blog options
		add_filter( 'option_blog_public', [ $this, 'option_blog_public' ], 100 );

		if ( ic_is_development() || ic_is_local() ) {
			set_error_handler( [ $this, 'error_handler' ] );
		}
	}

	/**
	 * @param int    $errno
	 * @param string $errstr
	 * @param string $errfile
	 * @param int    $errline
	 *
	 * @return bool
	 */
	public function error_handler( $errno, $errstr, $errfile, $errline ) {
		if ( ! ( error_reporting() & $errno ) ) {
			return false;
		}

		echo '<div style="top:0;left:0;position: fixed;z-index: 100000;width: 100%;height: 100%;background: red;padding: 80px;text-align: center;color: black;justify-content: center;align-items: center;display: flex;">' . $errstr . ' in ' . $errfile . ':' . $errline . '</div>';

		return true;
	}

	/**
	 * @param $option
	 *
	 * @return int
	 */
	public function option_blog_public( $option ) {
		if ( ic_is_production() ) {
			return $option;
		}

		return 0;
	}

	/**
	 * @param string[] $headers
	 *
	 * @return string[]
	 */
	public function wp_headers( $headers ) {
		if ( ! ic_is_production() ) {
			$headers['X-Robots-Tag'] = 'noindex, nofollow';
		}

		$headers['Server']       = 'Unknown';
		$headers['X-Powered-By'] = 'Unknown';

		return $headers;
	}

	/**
	 * Basic Auth
	 */
	public function template_redirect() {
		if ( ic_is_production() || ic_is_local() ) {
			return;
		}

		if ( is_user_logged_in() ) {
			return;
		}

		define( 'DONOTCACHEPAGE', true );

		nocache_headers();

		$has_supplied_credentials = ! ( empty( $_SERVER['PHP_AUTH_USER'] ) && empty( $_SERVER['PHP_AUTH_PW'] ) );

		$is_not_authenticated = (
			! $has_supplied_credentials ||
			$_SERVER['PHP_AUTH_USER'] !== $this->auth_login ||
			$_SERVER['PHP_AUTH_PW'] !== $this->auth_pass
		);

		if ( $is_not_authenticated ) {
			header( 'WWW-Authenticate: Basic realm="Access denied"' );

			wp_die( __( 'Sorry, you are not allowed to access this page.' ) . ' <a href="' . esc_url( wp_login_url( admin_url() ) ) . '">' . __( 'Log in' ) . '</a>', '', [ 'response' => 401 ] );
		}
	}

	/**
	 * @param WP_Admin_Bar $wp_admin_bar
	 */
	public function admin_bar_menu( $wp_admin_bar ) {
		if ( ic_is_local() ) {
			return;
		}

		$wp_admin_bar->remove_menu( 'updates' );
	}

	/**
	 * WP Mail args filter
	 *
	 * @param array $args
	 *
	 * @return mixed
	 */
	public function wp_mail( $args ) {
		if ( ic_is_production() ) {
			return $args;
		}

		if ( defined( 'WP_MAIL_TEST' ) && ! empty( WP_MAIL_TEST ) ) {
			$args['to'] = WP_MAIL_TEST;
		}

		$args['subject'] = strtoupper( wp_get_environment_type() ) . ': ' . $args['subject'];

		return $args;
	}

	/**
	 * Filter the list of rewrite rules formatted for output to an .htaccess file.
	 *
	 * @param string $rules mod_rewrite Rewrite rules formatted for .htaccess.
	 * @url http://stevegrunwell.github.io/wordpress-git/#/13
	 *
	 * @return string
	 */
	function mod_rewrite_rules( $rules ) {

		if ( ! defined( 'WP_REMOTE_URL' ) ) {
			return $rules;
		}

		$rules = "<IfModule mod_rewrite.c>
	RewriteEngine on
	RewriteCond %{REQUEST_FILENAME} !-d
	RewriteCond %{REQUEST_FILENAME} !-f
	RewriteRule wp-content/uploads/(.*) " . untrailingslashit( WP_REMOTE_URL ) . "/wp-content/uploads/$1 [NC,L]
</IfModule>\n\n" . $rules;

		return $rules;
	}

	/**
	 * @global WP_Admin_Bar $wp_admin_bar
	 */
	function wp_before_admin_bar_render() {
		/** @var WP_Admin_Bar $wp_admin_bar */
		global $wp_admin_bar;

		if ( ! ic_is_local() ) {
			$wp_admin_bar->remove_menu( 'updates' );
		}
	}

	/**
	 * Caps filter
	 *
	 * @param array   $allcaps An array of all the user's capabilities.
	 * @param array   $caps    Actual capabilities for meta capability.
	 * @param array   $args    Optional parameters passed to has_cap(), typically object ID.
	 * @param WP_User $user    The user object.
	 *
	 * @return array
	 */
	public function user_has_cap( $allcaps, $caps, $args, $user ) {
		if ( ! is_admin() ) {
			return $allcaps;
		}

		//Disable edit plugins and themes for all
		$allcaps['edit_plugins'] = 0;
		$allcaps['edit_themes']  = 0;

		//Disable update, install and switch themes
		$allcaps['update_themes']  = 0;
		$allcaps['install_themes'] = 0;
		//$allcaps['switch_themes']  = 0;

		//disable update, delete, install plugins and update core for other env
		if ( ! ic_is_local() ) {
			$allcaps['update_plugins']  = 0;
			$allcaps['delete_plugins']  = 0;
			$allcaps['install_plugins'] = 0;
			$allcaps['update_core']     = 0;
		}

		return $allcaps;
	}
}

new IC_Environment;