<?php

/**
 * Class IC_Page_Template
 */
class IC_Page_Template {
	/**
	 *
	 */
	public function add_hooks() {
		add_filter( 'manage_pages_columns', [ $this, 'add_column' ] );
		add_filter( 'default_hidden_columns', [ $this, 'add_hidden_column' ], 10, 2 );
		add_action( 'manage_pages_custom_column', [ $this, 'column_content' ], 10, 2 );
	}

	/**
	 * Filters the list of hidden columns.
	 *
	 * @param string[]  $hidden Array of IDs of hidden columns.
	 * @param WP_Screen $screen WP_Screen object of the current screen.
	 *
	 * @return string[]
	 */
	public function add_hidden_column( $hidden, $screen ) {
		if ( 'edit-page' === $screen->id ) {
			$hidden[] = 'page-layout';
		}

		return $hidden;
	}

	/**
	 * Filters the columns displayed in the Pages list table.
	 *
	 * @param string[] $post_columns An associative array of column headings.
	 *
	 * @return string[]
	 */
	public function add_column( $post_columns ) {
		$post_columns['page-layout'] = __( 'Template' );

		return $post_columns;
	}

	/**
	 * Fires in each custom column on the Posts list table.
	 *
	 * @param string $column_name The name of the column to display.
	 * @param int    $post_id     The current post ID.
	 */
	public function column_content( $column_name, $post_id ) {
		if ( 'page-layout' === $column_name ) {
			$templates = array_flip( get_page_templates() );

			echo $templates[ get_page_template_slug( $post_id ) ] ?? __( 'Default' );
		}
	}
}

( new IC_Page_Template )->add_hooks();