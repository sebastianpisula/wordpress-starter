<?php
/**
 * Plugin Name: Better Security
 */

class IC_Security {
	private $headers = 'mod_rewrite';

	/**
	 * IC_Security constructor.
	 */
	public function __construct() {
		add_filter( 'nocache_headers', [ $this, 'nocache_headers' ] );

		//Contact Form 7 Nonce
		//add_filter( 'wpcf7_verify_nonce', '__return_true' );
		add_filter( 'wpcf7_form_hidden_fields', [ $this, 'wpcf7_form_hidden_fields' ] );

		//Nonce for guests
		add_filter( 'nonce_user_logged_out', [ $this, 'nonce_user_logged_out' ] );

		//XMLRPC Disable
		add_filter( 'xmlrpc_enabled', '__return_false' );

		//Remove File Version
		add_filter( 'style_loader_src', [ $this, 'loader_src' ] );
		add_filter( 'script_loader_src', [ $this, 'loader_src' ] );

		//Remove WP-Version info
		remove_action( 'wp_head', 'wp_generator' );
		add_filter( 'the_generator', '__return_empty_string' );

		//Security Headers
		add_filter( 'wp_headers', [ $this, 'wp_headers' ] );
		add_filter( 'mod_rewrite_rules', [ $this, 'mod_rewrite_rules' ] );

		//Block Files
		add_action( 'generate_rewrite_rules', [ $this, 'generate_rewrite_rules' ] );

		add_action( 'template_redirect', [ $this, 'template_redirect' ], 0 );

		//Rest API
		remove_action( 'template_redirect', 'rest_output_link_header', 11 );

		add_filter( 'rest_endpoints', [ $this, 'rest_endpoints' ] );
	}

	public function template_redirect() {
		global $sitepress;

		if ( $sitepress instanceof SitePress ) {
			remove_action( 'wp_head', [ $sitepress, 'meta_generator_tag' ] );
		}
	}

	public function rest_endpoints( $endpoints ) {
		if ( is_user_logged_in() ) {
			return $endpoints;
		}

		if ( isset( $endpoints['/wp/v2/users'] ) ) {
			unset( $endpoints['/wp/v2/users'] );
		}
		if ( isset( $endpoints['/wp/v2/users/(?P<id>[\d]+)'] ) ) {
			unset( $endpoints['/wp/v2/users/(?P<id>[\d]+)'] );
		}

		return $endpoints;
	}

	/**
	 * Filters the cache-controlling headers.
	 *
	 * @param array $headers
	 *
	 * @return array
	 */
	public function nocache_headers( $headers ) {
		$headers['Pragma']        = 'no-cache';
		$headers['Cache-Control'] = 'no-cache, no-store, must-revalidate';

		return $headers;
	}

	public function wpcf7_form_hidden_fields( $fields ) {
		$fields['_ip'] = '';

		return $fields;
	}

	/**
	 * Security
	 *
	 * @return string
	 */
	public function nonce_user_logged_out() {
		return $_SERVER['REMOTE_ADDR'];
	}

	/**
	 * Filters the list of rewrite rules formatted for output to an .htaccess file.
	 *
	 * @param string $rules mod_rewrite Rewrite rules formatted for .htaccess.
	 *
	 * @return string
	 */
	public function mod_rewrite_rules( $rules ) {
		if ( ! in_array( $this->headers, [ 'mod_rewrite', 'all' ] ) ) {
			return $rules;
		}

		$rules = "#Security Headers\n<IfModule mod_headers.c>
	Header set X-XSS-Protection \"1; mode=block\"
	Header set X-Frame-Options \"SAMEORIGIN\"
	Header set Strict-Transport-Security \"max-age=31536000; includeSubDomains\"
	Header set X-Content-Type-Options nosniff
	Header set Referrer-Policy \"strict-origin-when-cross-origin\"
</IfModule>\n\n" . $rules;

		return $rules;
	}

	/**
	 * @param string $src
	 *
	 * @return string
	 */
	public function loader_src( $src ) {
		global $wp_version;

		$url = parse_url( $src );

		if ( isset( $url['query'] ) ) {
			parse_str( $url['query'], $query );

			if ( isset( $query['ver'] ) && $query['ver'] == $wp_version ) {
				return remove_query_arg( array( 'ver' ), $src );
			}
		}

		return $src;
	}

	/**
	 * Set security headers
	 *
	 * @param array $headers
	 *
	 * @return array
	 */
	function wp_headers( $headers ) {
		if ( ! ic_is_production() ) {
			$headers['Pragma']        = 'no-cache';
			$headers['Cache-Control'] = 'no-cache, no-store, must-revalidate';
		}

		if ( ! in_array( $this->headers, [ 'php', 'all' ] ) ) {
			return $headers;
		}

		//X-Frame-Options
		//$headers['Content-Security-Policy'] = 'frame-ancestors ' . implode( ' ', [] );

		$headers['X-Frame-Options'] = 'SAMEORIGIN';

		//X-XSS-Protection
		$headers['X-XSS-Protection'] = '1; mode=block';

		//Strict-Transport-Security
		if ( is_ssl() ) {
			$headers['Strict-Transport-Security'] = 'max-age=31536000; includeSubDomains';
		}

		//X-Frame-Options
		$headers['X-Frame-Options'] = 'SAMEORIGIN';

		//X-Content-Type-Options
		$headers['X-Content-Type-Options'] = 'nosniff';

		return $headers;
	}

	/**
	 * @param WP_Rewrite $wp_rewrite
	 */
	function generate_rewrite_rules( $wp_rewrite ) {
		$files = [
			'replace/(.*)',
			'wp-content/mu-plugins/(.*)',
			'wp-content/(.*)/readme\.(html|txt|md)',
			'wp-content/(plugins|themes|uploads)/(.*)\.php',
			'wp-content/debug\.log',
			'wp-config.php',
			'.htaccess',
			'wp-config-sample.php',
			'readme.md',
			'readme.txt',
			'readme.html',
			'xmlrpc.php',
			'license.txt',
			'.gitignore',
			'wp-admin/install.php',
			'wp-comments-post.php',
		];

		if ( ic_is_production() ) {
			$files[] = 'log.php';
		}

		foreach ( $files as $file ) {
			$wp_rewrite->add_external_rule( $file, 'index.php' );
		}
	}
}

new IC_Security();