<?php

add_filter( 'date_i18n', function ( string $date ): string {
	$months = [
		'/Styczeń/'     => 'stycznia',
		'/Luty/'        => 'lutego',
		'/Marzec/'      => 'marca',
		'/Kwiecień/'    => 'kwietnia',
		'/Maj/'         => 'maja',
		'/Czerwiec/'    => 'czerwca',
		'/Lipiec/'      => 'lipca',
		'/Sierpień/'    => 'sierpnia',
		'/Wrzesień/'    => 'września',
		'/Październik/' => 'października',
		'/Listopad/'    => 'listopada',
		'/Grudzień/'    => 'grudnia',
	];

	if ( preg_match( '/^\d{1,2}\ \w+/', $date ) ) {
		return preg_replace( array_keys( $months ), array_values( $months ), $date );
	}

	return $date;
} );
