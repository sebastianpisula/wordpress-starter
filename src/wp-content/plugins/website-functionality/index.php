<?php

/**
 * Plugin Name: Website Functionality
 * Description: Core functionality for website.
 * Version: 1.0.0
 */

namespace Website_Functionality;

include 'vendor/autoload.php';

( new Plugin( __FILE__ ) )->add_hooks();